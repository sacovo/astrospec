#!python
from setuptools import setup, find_packages
setup(
    name = "Astrospec",
    version = "0.1",
    packages = find_packages(),

    # Project uses reStructuredText, so ensure that the docutils get
    # installed or upgraded on the target machine
    install_requires = ['numpy>=1.9.2', 'pyfits>=3.3', 'matplotlib>=1.4.2', 'astropy>=1.1.2', 'scipy>=0.14.0'],

    package_data = {
    # If any package contains *.txt or *.rst files, include them:
    '': ['*.txt', '*.glade'],
    },

    # metadata for upload to PyPI
    author = "Sandro Covo",
    author_email = "sandro@covo.ch",
    description = "This is an Example Package",
    license = "GPLv3",
    keywords = "hello world example examples",
    url = "http://example.com/HelloWorld/",   # project home page, if any
    # could also include long_description, download_url, classifiers, etc.
    entry_points={
        'gui_scripts':[
            'astrospec = astrospec.gui.convert:run',
        ]
    }
)
