from gi.repository import Gtk
import matplotlib
from .. import image
from .. import settings
from pkg_resources import resource_string
matplotlib.use('Agg')
from matplotlib.figure import Figure
from matplotlib.patches import Rectangle
from matplotlib.backends.backend_gtk3agg import FigureCanvasGTK3Agg as FigureCanvas
from matplotlib.backends.backend_gtk3 import NavigationToolbar2GTK3 as NavigationToolbar
from matplotlib import cm # colormap
from matplotlib import pylab
from matplotlib import rc
import os
pylab.hold(False) # This will avoid memory leak

def get_cmap():
    return settings.config['DEFAULT']['colormap']


class Data:
    def __init__(self, application):
        self.app = application
        self.calib = image.Calibration()
        self.cropped = False

    def save(self, path):
        with  open(path, 'w') as f:
            for line in zip(*self.plot):
                print(line[0], line[1], sep=', ', file=f)
            f.close()

    def generate_plot(self):
        if not self.cropped:
            self.fit.crop(self.calib)
        self.cropped = True
        self.fit.get_plot()
        self.fit.calibrate(self.calib)
        x, y = self.fit.plot
        self.plot = [x, y]


    def load_fit(self, path):
        if self.app.gui.black_switch.get_state():
            self.fit = image.FITSImage(path, self.app.data.calib, filter=True)
        else:
            self.fit = image.FITSImage(path, filter=True)
        self.name = os.path.basename(path)
        self.cropped = False

    def load_blackrad(self, path):
        self.calib.load_blackradiation(path, self.app.data.calib)

    def load_calib(self, path):
        self.calib.load_calibration(path, self.app.data.calib)

    def load_calib_plot(self):
        p = self.calib.calib.get_plot()
        self.calib_plot = lambda : self.calib.calib.plot

    def get_image(self):
        return self.fit.data

    def get_plot(self):
        return self.plot

class Settings:
    def __init__(self, application):
        self.app = application

        settings.read_config()
        self.config = settings.config['DEFAULT']

    def fill_methods(self):
        mt_box = self.app.gui.mt_box
        methods = list(image.method_dict.keys())
        for mt in methods:
            mt_box.append(mt, mt)
        mt_box.set_active(methods.index(self.config['method']))

    def fill_filters(self):
        f_box = self.app.gui.f_box
        filters = list(image.filter_dict.keys())
        for f in filters:
            f_box.append(f, f)
        f_box.set_active(filters.index(self.config['filter']))

    def fill_ln(self):
        ln_box = self.app.gui.ln_box
        line_types = ['solid', 'dashed', 'dash-dot', 'dotted'];
        for ln in line_types:
            ln_box.append(ln, ln)
        ln_box.set_active(line_types.index(self.config['linetype']))

    def fill_cm(self):
        cm_box = self.app.gui.cm_box
        color_maps = sorted([str(c) for c in cm.datad])

        for cmap in color_maps:
            cm_box.append(cmap, cmap)
        cm_box.set_active(color_maps.index(self.config['colormap']))


    def fill_colors(self):
        fg_box = self.app.gui.fg_box
        bg_box = self.app.gui.bg_box
        colors = ['blue', 'green', 'red', 'cyan', 'magenta', 'yellow', 'black', 'white']
        for color in colors:
            fg_box.append(color, color)
            bg_box.append(color, color)
        fg_box.set_active(colors.index(self.config['color']))
        bg_box.set_active(colors.index(self.config['background']))

    def save_settings(self):
        settings.save_config()

    def apply_settings(self):
        cm = self.app.gui.cm_box.get_active_text()
        bg = self.app.gui.bg_box.get_active_text()
        fg = self.app.gui.fg_box.get_active_text()
        ln = self.app.gui.ln_box.get_active_text()
        method = self.app.gui.mt_box.get_active_text()
        self.config['colormap'] = cm
        self.config['background'] = bg
        self.config['color'] = fg
        self.config['linetype'] = ln
        self.config['method'] = method
        self.save_settings()
        self.app.gui.update_bg()
        self.app.gui.calib.update_bg()


class CalibWindow:
    def __init__(self, application, builder):
        self.app=application
        self.peak_box = builder.get_object('peak_box')

        self.ci_figure = Figure(figsize=(100, 100), dpi=75)
        self.ci_axis = self.ci_figure.add_subplot(111)
        self.ci_axis.set_axis_bgcolor('white')
        self.ci_canvas = FigureCanvas(self.ci_figure)

        self.bi_figure = Figure(figsize=(100, 100), dpi=75)
        self.bi_axis = self.bi_figure.add_subplot(111)
        self.bi_axis.set_axis_bgcolor('white')
        self.bi_canvas = FigureCanvas(self.bi_figure)

        self.b_figure = self.bi_figure #Figure(figsize=(100, 100), dpi=75)
        self.b_axis = self.bi_axis.twinx()
        self.b_axis.set_axis_bgcolor('white')
        self.b_canvas = self.bi_canvas

        self.c_figure = self.ci_figure #Figure(figsize=(100, 100), dpi=75)
        self.c_axis = self.ci_axis.twinx()
        self.c_canvas = self.ci_canvas
        self.ci_axis.set_axis_bgcolor(self.app.settings.config['background'])

        bi_toolbar = NavigationToolbar(self.bi_canvas, builder.get_object("fitdialog"))
        builder.get_object('bi_box').pack_start(bi_toolbar, False, False, 0)
        builder.get_object('bi_box').pack_start(self.bi_canvas, True, True, 0)

        self.flatfield = builder.get_object('absorption_switch')

        ci_toolbar = NavigationToolbar(self.ci_canvas, builder.get_object("fitdialog"))
        builder.get_object('ci_box').pack_start(ci_toolbar, False, False, 0)
        builder.get_object('ci_box').pack_start(self.ci_canvas, True, True, 0)

        self.peak_label = builder.get_object('peak_label')

        #builder.get_object('cp_box').pack_start(self.c_canvas, True, True, 0)

    def update_bg(self):
        self.ci_axis.set_axis_bgcolor(self.app.settings.config['background'])
        self.ci_canvas.draw_idle()

    def load_calib(self):
        self.ci_axis.imshow(self.app.data.calib.calib.image, origin='lower', cmap=cm.get_cmap(get_cmap()))
        fits = self.app.data.calib.calib
        low = fits.get_up()
        height = fits.width
        width = fits.image.shape[1]
        self.ci_axis.add_patch(Rectangle((0, low), width, height, fill=False))
        self.ci_canvas.draw_idle()

    def redraw_calib(self):
        self.ci_axis.clear()
        self.c_axis.clear()
        #self.load_calib()
        self.plot_calib()

    def plot_qe_factors(self):
        x = self.app.data.calib.bbp[0]
        factors = self.app.data.calib.factors
        self.b_axis.cla()
        self.b_axis.clear()
        self.b_axis.plot(x, factors)
        self.b_canvas.draw_idle()


    def redraw_blackrad(self):
        pass

    def plot_blackrad(self):
        x, data = self.app.data.calib.bbp
        self.app.data.calib.blackrad.get_plot()
        self.bi_axis.cla()
        self.bi_axis.clear()
        print(x, data)

        self.b_axis.plot(x, data, x, self.app.data.calib.blackrad.plot[1],
                         color=self.app.settings.config['color'],
                         linestyle=self.app.settings.config['linetype'])
        self.b_axis.set_xlim((x[0], x[-11]))
        self.b_canvas.draw_idle()


    def plot_calib(self):
        x1, data = self.app.data.calib_plot()
        self.c_axis.plot(x1, data, (0, len(x1)),(self.app.data.calib.threshold, self.app.data.calib.threshold),
                         color=self.app.settings.config['color'],
                         linestyle=self.app.settings.config['linetype'])
        self.c_axis.set_xlim((0, len(x1)))
        self.c_canvas.draw_idle()

    def count_peaks(self):
        count = len(self.app.data.calib.peak_dict.keys())
        self.peak_label.set_text('{} Peaks'.format(count))

    def load_blackrad(self):
        self.bi_axis.imshow(self.app.data.calib.blackrad.image, origin='lower', cmap=cm.get_cmap(get_cmap()))
        self.bi_canvas.draw_idle()

    def update_peaks(self):
        peaks = self.app.data.calib.peaks
        self.redraw_calib()
        d = -0.1
        if not self.app.gui.calib.flatfield.get_active():
            self.c_axis.set_ylim((0, 1.2))
            d = 0.1
        for widget in self.peak_box.get_children():
            self.peak_box.remove(widget)
        for i, peak in enumerate(peaks):
            p = self.get_peak(peak, i)
            self.c_axis.annotate(s=i, xy=(peak, self.app.data.calib.value_at(peak)),
                    xytext=(peak, self.app.data.calib.value_at(peak) + d),
                    arrowprops=dict(facecolor='black', arrowstyle='->'),rotation='vertical')
            self.peak_box.pack_start(p, True, True, 0)
            p.show()
        self.c_canvas.draw_idle()

    def draw_wave_fit(self):
        self.c_axis.clear()
        x= self.app.data.calib.x
        y = self.app.data.calib.y
        xn = self.app.data.calib.old_x
        yn = self.app.data.calib.ynew
        self.c_axis.plot(x, y, 'x', xn, yn, '-')
        self.c_canvas.draw_idle()

    def get_peak(self, peak, nr):
        box = Gtk.Box()
        label = Gtk.Label('{}'.format(nr))
        entry = Gtk.Entry()
        entry.set_input_purpose(Gtk.InputPurpose.NUMBER)
        box.pack_start(label, True, True, 4)
        box.pack_start(entry, False, False, 4)
        entry.set_width_chars(7)
        entry.set_max_width_chars(7)
        entry.connect('changed', self.app.action.on_peak_entry_changed, peak)
        entry.show()
        label.show()

        return box


class Gui:
    def __init__(self, application):
        self.app = application

        builder = Gtk.Builder()
        builder.add_from_string(resource_string(__name__, 'convert.glade').decode())
        builder.connect_signals(self.app.action)

        self.window = builder.get_object("window")

        self.cm_store = builder.get_object("cm_store")
        self.ln_store = builder.get_object("ln_store")
        self.color_store = builder.get_object("color_store")

        self.settings_dialog = builder.get_object("settings_dialog")
        self.settings_dialog.set_transient_for(self.window)
        self.fitdialog = builder.get_object("fitdialog")

        self.about = builder.get_object("aboutdialog")
        self.about.set_transient_for(self.window)

        self.figure = Figure(figsize=(100, 250), dpi=75)
        self.axis = self.figure.add_subplot(111)
        self.axis.set_axis_bgcolor(self.app.settings.config['background'])
        self.canvas = FigureCanvas(self.figure)

        self.i_figure = Figure(figsize=(100,100), dpi=75)
        self.i_axis = self.i_figure.add_subplot(111)
        self.i_canvas = FigureCanvas(self.i_figure)

        self.load_plot = builder.get_object("analyse_plot")
        self.percent_slider = builder.get_object("percent_slider")
        self.calib = CalibWindow(application, builder)
        self.title = builder.get_object("title-entry")

        self.black_switch = builder.get_object("black_switch")

        toolbar = NavigationToolbar(self.canvas, self.window)
        i_toolbar = NavigationToolbar(self.i_canvas, self.window)

        builder.get_object("image").pack_start(i_toolbar, False, False, 0)
        builder.get_object("image").pack_start(self.i_canvas, True, True, 0)

        builder.get_object("nav_box").pack_start(toolbar, False, False, 0)
        builder.get_object("plot").pack_start(self.canvas, True, True, 0)

        self.cm_box = builder.get_object('cm_box')
        self.bg_box = builder.get_object('bg_box')
        self.fg_box = builder.get_object('fg_box')
        self.ln_box = builder.get_object('ln_box')
        self.mt_box = builder.get_object('mt_box')
        self.f_box = builder.get_object('f_box')
        self.color_temp = builder.get_object('color_temp')
        self.factor = builder.get_object('factor_entry')

        for i in range(1, 4):
            builder.get_object('entry' + str(i) ).set_text(self.app.settings.config['f_param'+ str(i)])

    def update_bg(self):
        self.axis.set_axis_bgcolor(self.app.settings.config['background'])
        self.canvas.draw_idle()

    def show(self):
        self.window.show_all()

    def load_image(self):
        self.i_axis.imshow(self.app.data.get_image(), cmap=self.app.settings.config['colormap'], origin='lower')
        fits = self.app.data.fit
        low = fits.get_up()
        height = fits.width
        width = fits.image.shape[1]
        self.i_axis.add_patch(Rectangle((0, low), width, height, fill=False))

        self.i_canvas.draw_idle()
        self.load_plot.set_sensitive(True)

    def blackbody_stats(self):
        return (self.color_temp.get_value(), float(self.factor.get_text()))

    def load_blackrad(self):
        self.calib.load_blackrad()

    def set_title(self, title):
        self.axis.set_title(title)
        self.canvas.draw_idle()

    def set_x(self, x):
        self.axis.set_xlabel(x)
        self.canvas.draw_idle()

    def set_y(self, y):
        self.axis.set_ylabel(y)
        self.canvas.draw_idle()

    def plot(self):
        x, y = self.app.data.get_plot()
        x_axis = 'Pixel'
        if self.app.data.calib.xnew != None:
            x_axis = "Wellenlänge [\AA]"
        self.axis.plot(x, y, color=self.app.settings.config['color'],
                linestyle=self.app.settings.config['linetype'])
        self.axis.set_xlim([x[0], x[-1]])
        self.axis.set_xlabel(x_axis)
        self.axis.set_ylabel("Rel. Intensität")
        self.axis.grid()
        self.canvas.draw_idle()

    def load_calib(self):
        self.calib.load_calib()

class ActionHandler:

    def on_load_black_image_clicked(self, widget):
        path = self.get_path(self.app.gui.window, Gtk.FileChooserAction.OPEN, 'Open Black image',
                Gtk.STOCK_OPEN)
        self.app.data.calib.load_black_image(path)

    def on_save_fit_clicked(self, widget):
        path = self.get_path(self.app.gui.window, Gtk.FileChooserAction.SAVE, 'Save Calibration', Gtk.STOCK_SAVE)
        image.save_calib(self.app.data.calib, path)

    def on_load_fit_clicked(self, widget):
        path = self.get_path(self.app.gui.window, Gtk.FileChooserAction.OPEN, 'Open Calibration',
                Gtk.STOCK_OPEN)
        self.app.data.calib = image.load_calib(path)

    def on_title_entry_changed(self, widget):
        self.app.gui.set_title(widget.get_text())

    def on_y_entry_changed(self, widget):
        self.app.gui.set_y(widget.get_text())

    def on_x_entry_changed(self, widget):
        self.app.gui.set_x(widget.get_text())

    def on_peak_entry_changed(self, widget, value):
        if widget.get_text():
            self.app.data.calib.update_peak(value, float(widget.get_text()))
        else:
            self.app.data.calib.remove_peak(value)
        self.app.gui.calib.count_peaks()

    def __init__(self, application):
        self.app = application

    def write_csv(file_, data, headers=None):
        if headers:
            file_.write(('{}'*len(headers)).format(*headers))
        for line in data:
            file_.write(('{}'*len(data)).format(*line))
        file_.close()

    def on_settings_menu_activate(self, widget, data=None):
        self.app.gui.settings_dialog.run()

    def on_accept_settings_clicked(self, widget, data=None):
        try:
            self.app.settings.apply_settings()
            self.app.gui.settings_dialog.hide()
        except TypeError:
            return

    def on_cancel_settings_clicked(self, widget, data=None):
        self.app.gui.settings_dialog.hide()

    def get_path(self, window, mode, title, accept):
        fcd = Gtk.FileChooserDialog(title, window, mode,
                                            (Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                                             accept, Gtk.ResponseType.ACCEPT))
        response = fcd.run()
        path = ''
        if response == Gtk.ResponseType.ACCEPT:
            path = fcd.get_file().get_path()
        fcd.hide()
        return path

    def on_save_plot_clicked(self, widget, data=None):
        path = self.get_path(self.app.gui.window, Gtk.FileChooserAction.SAVE, 'Save CSV', Gtk.STOCK_SAVE)
        if path:
            self.app.data.save(path)

    def on_file_open_activate(self, widget, data=None):
        path = self.get_path(self.app.gui.window, Gtk.FileChooserAction.OPEN, 'Open FIT', Gtk.STOCK_OPEN)
        if path:
            self.app.data.load_fit(path)
            self.app.gui.load_image()

    def on_window_destroy(self, widget, data=None):
        self.app.settings.save_settings()
        Gtk.main_quit()

    def on_about_mnu_activate(self, widget, aboutdialog=None):
        self.app.gui.about.run()
        self.app.gui.about.hide()

    def on_analyse_plot_clicked(self, widget, data=None):
        self.app.data.generate_plot()
        self.app.gui.load_image()
        self.app.gui.plot()

    def on_create_fit_clicked(self, widget, data=None):
        self.app.gui.fitdialog.show_all()
        response = self.app.gui.fitdialog.run()
        self.app.gui.fitdialog.hide()
        if response == -1:
            self.on_create_fit_clicked(widget)
        if response == 0:
            pass

    def on_black_rad_load_clicked(self, widget, data=None):
        path = self.get_path(self.app.gui.window, Gtk.FileChooserAction.OPEN, 'Open FIT', Gtk.STOCK_OPEN)
        if path:
            self.app.data.load_blackrad(path)
            value = self.app.data.calib.func_filter()
            self.app.gui.percent_slider.set_value(value)
            self.app.gui.load_blackrad()

    def on_calib_load_clicked(self, widget, data=None):
        path = self.get_path(self.app.gui.window, Gtk.FileChooserAction.OPEN, 'Open FIT', Gtk.STOCK_OPEN)
        if path:
            self.app.data.load_calib(path)
            self.app.gui.load_calib()

    def on_black_rad_an_clicked(self, widget, data=None):
        self.app.data.calib.calib_blackrad()
        self.app.gui.load_blackrad()

    def on_percent_slider_change_value(self, widget, data=None):
        self.app.data.calib.filter_percent(widget.get_value())
        self.app.gui.load_blackrad()

    def on_width_spin_change_value(self, widget, data=None):
        self.app.data.calib.calib.set_width(int(widget.get_value()))
        self.app.gui.load_calib()

    def on_pos_spin_change_value(self, widget, data=None):
        self.app.data.calib.calib.set_pos(int(widget.get_value()))
        self.app.gui.load_calib()

    def on_find_peaks_clicked(self, widget, data=None):
        self.app.data.load_calib_plot()
        self.app.gui.calib.plot_calib()

    def on_width_spin_main_value_changed(self, widget, data=None):
        self.app.data.fit.set_width(int(widget.get_value()))
        self.app.gui.load_image()

    def on_pos_spin_main_value_changed(self, widget, data=None):
        self.app.data.fit.set_pos(int(widget.get_value()))
        self.app.gui.load_image()

    def on_min_range_changed(self, widget, data=None):
        self.app.data.calib.threshold = widget.get_value()

    def on_max_range_changed(self, widget, data=None):
        self.app.data.calib.min_width = widget.get_value()

    def on_peak_button_clicked(self, widget, data=None):
        self.app.data.calib.find_peaks(absorption=self.app.gui.calib.flatfield.get_active())
        self.app.gui.calib.update_peaks()

    def on_fit_button_clicked(self, widget, data=None):
        self.app.data.calib.fit_wavelength_func()
        self.app.gui.calib.draw_wave_fit()

    def on_clear_button_clicked(self, widget, data=None):
        self.app.data.calib.clear()
        self.app.gui.calib.count_peaks()

    def on_flatfield_button_clicked(self, widget, data=None):
        self.app.data.calib.flat_func()
        self.app.gui.calib.plot_calib()

    def on_param_changed(self, widget):
        n = widget.get_placeholder_text()[-1]
        self.app.settings.config['f_param' + n] = widget.get_text()

    def on_plot_blackrad_button_clicked(self, widget):
        self.app.data.calib.blackbody_plot(*self.app.gui.blackbody_stats())
        self.app.gui.calib.plot_blackrad()

    def on_fit_qe_button_clicked(self, widget):
        self.app.data.calib.qe_detection()
        self.app.gui.calib.plot_qe_factors()

    def on_f_box_changed(self, widget):
        self.app.settings.config['filter'] = widget.get_active_text()

    def on_delete_calib_clicked(self, widget):
        self.app.data.calib.clear_calib()


class Application:
    def __init__(self):
        self.action = ActionHandler(self)
        self.data = Data(self)
        self.settings = Settings(self)
        self.gui = Gui(self)

        self.settings.fill_cm()
        self.settings.fill_ln()
        self.settings.fill_colors()
        self.settings.fill_methods()
        self.settings.fill_filters()


    def plot_spectrum(self, spectrum):
        pass

    def main(self):
        self.gui.show()
        Gtk.main()


def run():
    app = Application()
    app.main()
