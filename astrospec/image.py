import cv2
import numpy as np
from scipy.interpolate import interp1d, splrep, splev
from scipy import signal
from scipy.ndimage.filters import gaussian_filter, median_filter, \
        gaussian_laplace, maximum_filter, minimum_filter, percentile_filter
import scipy.constants as c
from . import settings
from astropy.io import fits
import json

method_dict = {
    'linear': lambda x: x,
    'ln': np.log,
    'log2': np.log2,
    'log10': np.log10,
    'square': lambda x: x**2,
    'cubic': lambda x: x**3,
    'sqrt': np.sqrt,
    'sinh': np.sinh,
}

filter_dict = {
    'None': lambda data, p1, p2, p3: data,
    'Gauss': lambda data, p1, p2, p3: gaussian_filter(data, p1),
    'Median': lambda data, p1, p2, p3: median_filter(data, size=p1),
    'Guass Laplace': lambda data, p1, p2, p3: gaussian_laplace(data, sigma=p1),
    'Maximum': lambda data, p1, p2, p3: maximum_filter(data, size=p1),
    'Minimum': lambda data, p1, p2, p3: minimum_filter(data, size=p1),
    'Percintile': lambda data, p1, p2, p3: percintile_filter(data, percentile=p1, size=p2),
}

def angle_cos(p0, p1, p2):
    d1, d2 = (p0-p1).astype('float'), (p2-p1).astype('float')
    return abs(np.dot(d1, d2) / np.sqrt(np.dot(d1, d1)*np.dot(d2, d2)))

def rotate_image(img, angle, midpoint):
    rows,cols = img.shape
    M = cv2.getRotationMatrix2D(tuple(midpoint), angle, 1)
    dst = cv2.warpAffine(img,M,(cols,rows))
    return dst

def find_peaks(sig, method='cwt', **kwargs):
    if method == 'cwt':
        return signal.find_peaks_cwt(sig, np.arange(kwargs['arg1'], kwargs['arg2']))
    peaks = []
    threshold = kwargs['arg1']
    min_width = kwargs['arg2']
    if kwargs['absorption']:
        indexes = np.where(sig < threshold)[0]
    else:
        indexes = np.where(sig > threshold)[0]
    peaks = [[indexes[0]]]
    for index in indexes[1:]:
        if index == peaks[-1][-1] + 1:
            peaks[-1].append(index)
        else:
            peaks.append([index])
    peaks = [peak for peak in peaks if len(peak) > min_width]
    max_indexes = []
    for peak in peaks:
        cut = sig[peak[0]:peak[-1]]
        if kwargs['absorption']:
            max_indexes.append((np.where(cut == min(cut))[0]+peak[0])[0])
        else:
            max_indexes.append((np.where(cut == max(cut))[0]+peak[0])[0])
    return max_indexes

    for i in range(1, len(sig) - 1):
        c = sig[i]
        p = sig[i-1]
        n = sig[i+1]
        if p < c & c > n:
            peaks.append[i]
    return peaks

def calib(data):
    return method_dict[settings.config['DEFAULT']['method']](data)

def filter_(data):
    p1 = float(settings.config['DEFAULT']['f_param1'])
    p2 = float(settings.config['DEFAULT']['f_param2'])
    p3 = float(settings.config['DEFAULT']['f_param3'])
    p1 = p1 if p1 != -1 else None
    p2 = p2 if p2 != -1 else None
    p3 = p3 if p3 != -1 else None
    print(settings.config['DEFAULT']['filter'])
    print('filter_', data.max())
    return filter_dict[settings.config['DEFAULT']['filter']](data, p1, p2, p3)

def plancks_law(lam, T, factor):
    lam = np.array(lam) * factor
    result = 2.*np.pi*c.h*(c.c**2)/(lam**5)
    result /= (np.exp(c.h*c.c/(lam*c.k*T)) - 1.0)
    bbp = result / result.max()
    return bbp

class FITSImage:

    def __init__(self, path, calibration=None, filter=False):
        self.fits = fits.open(path)[0]
        self.data = self.fits.data.astype(np.int32)
        if calibration:
            self.data = calibration.dark_correction(self.data)
            print('calib',self.data.max())
        if filter:
            self.data = calib(filter_(self.data))
        print('after', self.data.max())
        self.width = 30
        self.pos = 0

        self.image = self.data.astype(np.uint16)
        self.cv_image = (self.image>>8).astype(np.uint8)

    def cheap_analyse(self):
        return [np.sum(d) for d in self.data.transpose()]

    def set_width(self, width):
        self.width = width
        self.update()

    def set_pos(self, pos):
        self.pos = pos
        self.update()

    def update(self):
        img = self.data.copy()
        r, c = img.shape
        #cv2.line(img, (0, self.get_up()), (c, self.get_up()),
        #        (2**16 -1), 10)
        #cv2.line(img, (0, self.get_low()), (c, self.get_low()),
        #        (2**16 -1 ), 10)
        self.image = img

    def get_up(self):
        return int((self.data.shape[0] /2 + self.pos) - self.width/2)

    def get_low(self):
        return int((self.data.shape[0] /2 + self.pos) + self.width/2)

    def get_plot(self):
        a = self.data[self.get_up():self.get_low()]
        a = a/a.max()
        x = np.arange(a.shape[1])
        self.plot = [x, a.mean(axis=0)]
        return self.plot

    def calibrate(self, calibration):
        self.plot = calibration.wavelength_corection(self.plot)
        self.plot = calibration.qe_correction(self.plot)

    def crop(self, calibration):
        self.image = calibration.crop_image(self.image)
        self.cv_image = calibration.crop_image(self.cv_image)
        self.data = calibration.crop_image(self.data.astype(np.uint16)).astype(np.int32)

    def substract(self, calibration):
        if calibration.black_image:
            self.data = self.data - calibration.black_image.data



class Calibration:

    def __init__(self):
        self.storage = CalibStorage()
        self.peak_dict = dict()
        self.xnew = None
        self.ynew = None
        self.threshold = 0.10
        self.lines = []
        self.near = []
        self.min_width = 5
        self.factors = None
        self.black_image = None

    def load_black_image(self, path):
        self.black_image = FITSImage(path)

    def dark_correction(self, data):
        if self.black_image != None:
            data = data - self.black_image.data
            data[data < 0] = 0
            return data
        return data

    def qe_correction(self, plot):
        if self.factors == None:
            return plot
        plot[1] = plot[1] * self.factors
        return plot

    def wavelength_corection(self, plot):
        print("Wavelength...")
        if self.xnew == None:
            return plot
        plot[0] = self.xnew
        if self.xnew[0] > self.xnew[-1]:
            plot[0] = plot[0][::-1]
            plot[1] = plot[1][::-1]
        return plot

    def load_blackradiation(self, path, calib):
        self.blackrad = FITSImage(path, filter=True)
        self.blackrad.substract(self)


    def load_calibration(self, path, calib):
        self.calib = FITSImage(path, filter=True)
        self.calib.substract(self)
        self.calib.crop(self)

    def calib_blackrad(self):
        self.find_extent()

    def func_filter(self):
        value = 70
        while len(self.lines) != 2 & value < 100:
            value += 1
            self.filter_percent(value)
        return value

    def filter_percent(self, value):
        img = self.blackrad.cv_image.copy()
        self.th = value
        x,y,w,h = cv2.boundingRect(self.bigest_cnt(img))
        img = cv2.rectangle(img,(x,y),(x+w,y+h),(255),5)
        self.blackrad.image = img
        self.rect = [x, y, w, h]

    def bigest_cnt(self, img):
        _, thresh = cv2.threshold(img.copy(), self.th, 255, cv2.THRESH_BINARY)
        _, cnts, _ = cv2.findContours(thresh.copy(), cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_SIMPLE)
        cnts = sorted(cnts, key=lambda v: cv2.contourArea(v), reverse=True)
        return cnts[0]


    def find_extent(self):
        img = self.blackrad.cv_image
        x, y, w, h = self.rect
        img = img[y:y + h, x:x + w]
        p, s, angle = cv2.minAreaRect(self.bigest_cnt(img))
        anlge = -angle
        if s[0] < s[1]:
            angle = 90 + angle
        img = rotate_image(img, angle, (w/2, h/2))
        rect = cv2.boundingRect(self.bigest_cnt(img))
        outer = [x, y, w, h]
        x2, y2, w2, h2 = rect
        img = img[y2:y2 + h2, x2:x2 + w2]
        inner = [x2, y2, w2, h2]

        self.blackrad.image = self.blackrad.data.astype(np.uint16)

        self.storage.angle = angle
        self.storage.outer = outer
        self.storage.inner = inner

        self.blackrad.crop(self)

    def crop_image(self, image):
        if not hasattr(self.storage, 'outer'):
            return image
        x, y, w, h = self.storage.outer
        img = image[y:y + h, x:x + w]
        img = rotate_image(img, self.storage.angle, (w/2, h/2))
        x, y, w, h = self.storage.inner
        img = img[y:y + h, x:x + w]
        return img

    def value_at(self, x):
        return self.calib.plot[1][x]

    def update_peak(self, pixel, wl):
        self.peak_dict[int(pixel)] = wl

    def remove_peak(self, pixel):
        del self.peak_dict[int(pixel)]

    def get_angle(self):
        pass

    def get_boundaries(self):
        pass

    def get_wavelength(self, pix_val):
        return self.interpolate(pix_val)

    def get_rel_intensity(self, wavelenth, pix_val):
        pass

    def blackbody_plot(self, t, factor):
        x = self.xnew
        if self.xnew[0] > self.xnew[-1]:
            x = x[::-1]
        bbp = plancks_law(x, t, factor)
        self.bbp = [x, bbp]
        return [x, bbp]

    def qe_detection(self):
        # Create a b-spline of the blackbody fit
        s = 10
        k = 5
        nest= -1

        self.blackrad.get_plot()
        self.blackrad.plot = self.wavelength_corection(self.blackrad.plot)

        tckp = splrep(self.blackrad.plot[0], self.blackrad.plot[1], xb=self.blackrad.plot[0][0],
                      xe=self.blackrad.plot[0][-1], k=k, s=s)
        near = splev(self.blackrad.plot[0], tckp)
        print(self.bbp)
        factors = self.bbp[1] / self.blackrad.plot[1]
        self.factors = factors
        self.storage.factors = factors.tolist()
        print(factors)


    def flat_func(self):
        s=10 # smoothness parameter
        k=5 # spline order
        nest=-1 # estimate of number of knots needed (-1 = maximal)

        # find the knot points
        tckp = splrep(self.calib.plot[0],self.calib.plot[1], xb=0, xe=self.calib.plot[0][-1], k=k, s=s)
        # evaluate spline, including interpolated points
        self.near = splev(self.calib.plot[0],tckp)
        print(self.near)
        self.calib.plot[1] = self.calib.plot[1] - self.near

    def find_peaks(self, absorption=False):
        self.peaks = find_peaks(self.calib.plot[1], method='', arg1=self.threshold, arg2=self.min_width, absorption=absorption)

    def clear(self):
        self.peak_dict.clear()

    def clear_calib(self):
        self.__init__()

    def fit_wavelength_func(self):
        # Interpolate...
        if self.peak_dict:
            xs = list(self.peak_dict.keys())
            xs.sort()
            ys = [self.peak_dict[x] for x in xs]

            self.storage.x = xs
            self.storage.y = ys
        elif not (self.storage.x and self.storage.y):
            return "Error"
        self.x = np.array(self.storage.x)
        self.y = np.array(self.storage.y)
        # spline parameters
        s=3.0 # smoothness parameter
        k=3 # spline order
        nest=-1 # estimate of number of knots needed (-1 = maximal)

        # find the knot points
        tckp = splrep(self.x,self.y, xb=0, xe=self.calib.plot[0][-1], k=1)

        # evaluate spline, including interpolated points
        xnew = self.calib.plot[0]
        ynew = splev(xnew,tckp)
        fu = interp1d(self.x, self.y)
        self.interpolate = fu
        self.old_x = xnew
        self.xnew = ynew
        self.ynew = ynew
        self.storage.w_points = self.ynew.tolist()
        with open('points.dat', mode='w') as f:
            for x, y in zip(xs, ys):
                f.write('{}{}\n'.format(x, y))

        with open('data.dat', mode='w') as f:
            for x, y, in zip(xnew, ynew):
                f.write('{}{}\n'.format(x, y))

        return fu


    def fit_intesity_func(self, black_dict):
        pass

class CalibStorage:
    def __init__(self):
        pass

def save_calib(calib, path):
    with open(path, 'w') as f:
        json.dump(calib.storage.__dict__, f)

def load_calib(path):
    with open(path, 'r') as f:
        d = json.load(f)
    cs = CalibStorage()
    cs.__dict__ = d
    calib = Calibration()
    calib.storage = cs
    calib.xnew = cs.w_points
    calib.factors = np.array(cs.factors)
    return calib
