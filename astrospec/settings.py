import configparser
import os

config = configparser.ConfigParser()
config_path = os.path.expanduser('~/.astrospec.conf')

config['DEFAULT'] = {
    'colormap': 'gray',
    'background': 'black',
    'color': 'green',
    'linetype': 'solid',
    'method': 'linear',
    'filter': 'None',
    'f_param1': 0,
    'f_param2': 0,
    'f_param3': 0,
}

def read_config():
    config.read(config_path)

def save_config():
    with open(config_path, 'w') as configfile:
        config.write(configfile)

